package test;

public class Materie {
	 long id;
	 String Denumire;
	 Profesor profesor;
	 
	 public boolean equals(Object obj)
	 {
		 if(this==obj)
		 {
			 return true;
		 }
		 if((obj==null)||(obj.getClass()!=this.getClass()))		 
		 {
			 return false;
		 }
		 Materie test=(Materie)obj;
		return ((this.id==test.id) &&
				((this.Denumire==test.Denumire)||(Denumire != null && Denumire.equals(test.Denumire)))
				&& this.profesor.equals(test.profesor));
						
	 }
	 public int hashCode()
	 {
		 int hash=7;
		 hash=31*hash+(int)id;
		 hash=31*hash + (null==Denumire ? 0 :Denumire.hashCode());
		 hash=31*profesor.hashCode();
		 return hash;
	 }
	 
}
