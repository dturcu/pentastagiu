package test;

public class Student {
	 long id;
	 String nume;
	 String prenume;
	 String Facultate;
	 int varsta;
	 int anDeStudiu;
	
	 
	 public boolean equals(Object obj)
	 {
		 if(this==obj)
		 {
			 return true;
		 }
		 if((obj==null)||(obj.getClass()!=this.getClass()))		 
		 {
			 return false;
		 }
		 Student test=(Student)obj;
		return ((this.id==test.id) && (this.varsta == test.varsta)) && 
				((this.nume==test.nume)||(nume != null && nume.equals(test.nume)))&&
						((this.prenume==test.prenume)||(prenume != null && prenume.equals(test.prenume)))&&
						(this.anDeStudiu==test.anDeStudiu)&&
						((this.Facultate==test.Facultate)||(Facultate != null && Facultate.equals(test.Facultate)))
						;
	 }
	 public int hashCode()
	 {
		 int hash=7;
		 hash=31*hash+(int)id;
		 hash=31*hash+varsta;
		 hash=31*hash+anDeStudiu;
		 hash=31*hash + (null==nume ? 0 :nume.hashCode());
		 hash=31*hash + (null==prenume ? 0:prenume.hashCode());
		 hash=31*hash + (null==Facultate ? 0:Facultate.hashCode());
		 return hash;
	 }
	 
}
