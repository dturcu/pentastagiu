package test;

public class Profesor {
 long id;
 String nume;
 String prenume;
 int varsta;
 
 public boolean equals(Object obj)
 {
	 if(this==obj)
	 {
		 return true;
	 }
	 if((obj==null)||(obj.getClass()!=this.getClass()))		 
	 {
		 return false;
	 }
	Profesor test=(Profesor)obj;
	return ((this.id==test.id) && (this.varsta == test.varsta)) && 
			((this.nume==test.nume)||(nume != null && nume.equals(test.nume)))&&
					((this.prenume==test.prenume)||(prenume != null && prenume.equals(test.prenume)));
 }
 public int hashCode()
 {
	 int hash=7;
	 hash=31*hash+(int)id;
	 hash=31*hash+varsta;
	 hash=31*hash + (null==nume ? 0 :nume.hashCode());
	 hash=31*hash + (null==prenume ? 0:prenume.hashCode());
	 return hash;
 }
 
}
