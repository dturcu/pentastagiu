package com.mycompany.app;

public class InscriereCursuri<K,V> {

	 private K TipPersoana;
	 private V Denumire;
	
	 public InscriereCursuri(K k , V v )
	 {
		 TipPersoana=k;
		 Denumire=v;
	 }
	 
	 public void afisareInscriere()
	 {
		 try
		 {
			 if(TipPersoana instanceof Student)
			 {
				 System.out.println(((Student) TipPersoana).nume + ((Student) TipPersoana).prenume + "este inscris la materia" + Denumire );
			 }
			 if(TipPersoana instanceof Profesor)
			 {
				 System.out.println(((Profesor) TipPersoana).nume + ((Profesor) TipPersoana).prenume + "este inscris la materia" + Denumire );
					
			 }
		 }		
		 catch(Exception e)
		 {
			 System.err.println("Class not as expected: " + e.getMessage());
		 }
		 finally{
		 
		 }
		
		
	 }
	
}
