package com.mycompany.app;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

public class Student {
	 long id;
	 String nume;
	 String prenume;
	 String Facultate;
	 int varsta;
	 int anDeStudiu;
	
	 public static void cat(File file) {
		    RandomAccessFile input = null;
		    String line = null;

		    try {
		        input = new RandomAccessFile(file, "r");
		        while ((line = input.readLine()) != null) {
		            System.out.println(line);
		        }
		        return;
		    } catch(FileNotFoundException fnf) {
		        System.err.format("File: %s not found%n", file);
		    } catch(IOException e) {
		        System.err.println(e.toString());
		    } finally {
		        if (input != null) {
		           
		                input.close();
		            
		        }
		    }
		}
	 
	 
	 
	 public boolean equals(Object obj)
	 {
		 if(this==obj)
		 {
			 return true;
		 }
		 if((obj==null)||(obj.getClass()!=this.getClass()))		 
		 {
			 return false;
		 }
		 Student test=(Student)obj;
		return ((this.id==test.id) && (this.varsta == test.varsta)) && 
				((this.nume==test.nume)||(nume != null && nume.equals(test.nume)))&&
						((this.prenume==test.prenume)||(prenume != null && prenume.equals(test.prenume)))&&
						(this.anDeStudiu==test.anDeStudiu)&&
						((this.Facultate==test.Facultate)||(Facultate != null && Facultate.equals(test.Facultate)))
						;
	 }
	 public int hashCode()
	 {
		 int hash=7;
		 hash=31*hash+(int)id;
		 hash=31*hash+varsta;
		 hash=31*hash+anDeStudiu;
		 hash=31*hash + (null==nume ? 0 :nume.hashCode());
		 hash=31*hash + (null==prenume ? 0:prenume.hashCode());
		 hash=31*hash + (null==Facultate ? 0:Facultate.hashCode());
		 return hash;
	 }
	 
}
