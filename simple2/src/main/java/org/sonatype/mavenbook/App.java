package org.sonatype.mavenbook;
import java.util.*;
import java.io.*;
/**
 * Hello world!
 *
 */
public class App 
{
	  public static void main(String[] args) {
	        final int buffersize = 80;
	        File fisier = new File(args[0]);
	        List<String> fileList = 
	            new ArrayList<String>((int)(fisier.length() / buffersize) * 2);
	        BufferedReader reader = null;
	        int lineCount = 0;
	        try {
	            reader = new BufferedReader(new FileReader(fisier));
	            for (String line = reader.readLine(); line != null;
	                    line = reader.readLine()) {
	                fileList.add(line);
	                lineCount++;
	            }
	        } catch (IOException e) {
	            System.err.format("Nu sa putut efectua citirea  %s: %s%n", fisier, e);
	            System.exit(1);
	        } finally {
	            if (reader != null) {
	                try {
	                    reader.close();
	                } catch (IOException e) {}
	            }
	        }
	        int repeats = Integer.parseInt(args[1]);
	        Random random = new Random();
	        for (int i = 0; i < repeats; i++) {
	            System.out.format("%d: %s%n", i,
	                    fileList.get(random.nextInt(lineCount - 1)));
	        }
	    }
}
